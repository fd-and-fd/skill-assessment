CREATE TABLE `invoicesystem`.`product` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` NVARCHAR(255) NOT NULL,
  `price` DOUBLE NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `invoicesystem`.`invoice` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `customer_name` NVARCHAR(255) NOT NULL,
  `issue_date` DATETIME NOT NULL,
  `due_date` DATETIME NOT NULL,
  `comment` MEDIUMTEXT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `invoicesystem`.`invoice_item` (
  `invoice_id` INT NOT NULL,
  `product_id` INT NOT NULL,
  `quantity` INT NOT NULL,
  PRIMARY KEY (`invoice_id`, `product_id`),
  INDEX `product_fk_idx` (`product_id` ASC) INVISIBLE,
  CONSTRAINT `invoice_fk`
    FOREIGN KEY (`invoice_id`)
    REFERENCES `invoicesystem`.`invoice` (`id`)
    ON DELETE RESTRICT
    ON UPDATE NO ACTION,
  CONSTRAINT `product_fk`
    FOREIGN KEY (`product_id`)
    REFERENCES `invoicesystem`.`product` (`id`)
    ON DELETE RESTRICT
    ON UPDATE NO ACTION);
