package dfdf.skillassessment.dfdfskillassessment.model.dto.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
public class CreateInvoiceDto {
    @NotBlank
    private String customerName;

    @NotNull
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
    private Date issueDate;

    @NotNull
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
    private Date dueDate;

    private String comment;
    @NotEmpty
    private List<CreateInvoiceItemDto> items;
}

