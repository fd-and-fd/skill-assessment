package dfdf.skillassessment.dfdfskillassessment.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class InvoiceItemId implements Serializable {

    @Column(name = "invoice_id")
    private int invoiceId;
    @Column(name = "product_id")
    private int productId;

    public InvoiceItemId(int invoiceId, int productId) {
        this.invoiceId = invoiceId;
        this.productId = productId;
    }
}
