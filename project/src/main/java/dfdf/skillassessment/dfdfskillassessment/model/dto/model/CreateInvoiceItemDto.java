package dfdf.skillassessment.dfdfskillassessment.model.dto.model;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
@Accessors(chain = true)
public class CreateInvoiceItemDto {

    @NotEmpty
    private String productName;

    @Min(0)
    private double priceHuf;

    @Min(0)
    private int quantity;
}
