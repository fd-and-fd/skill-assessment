package dfdf.skillassessment.dfdfskillassessment.model.dto.mapper;

import dfdf.skillassessment.dfdfskillassessment.model.dto.model.ProductDto;
import dfdf.skillassessment.dfdfskillassessment.model.entity.ProductEntity;

import java.util.ArrayList;
import java.util.List;

public class ProductMapper {
    public static ProductDto toProductDto(ProductEntity productEntity){
        return new ProductDto()
                .setId(productEntity.getId())
                .setPriceHuf(productEntity.getPrice())
                .setName(productEntity.getName());
    }

    public static List<ProductDto> toProductDto(Iterable<ProductEntity> productEntities){
        var products = new ArrayList<ProductDto>();
        productEntities.forEach(productEntity -> products.add(toProductDto(productEntity)));

        return products;
    }
}
