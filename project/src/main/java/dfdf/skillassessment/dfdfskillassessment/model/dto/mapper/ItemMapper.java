package dfdf.skillassessment.dfdfskillassessment.model.dto.mapper;

import dfdf.skillassessment.dfdfskillassessment.model.dto.model.ItemDto;
import dfdf.skillassessment.dfdfskillassessment.model.entity.InvoiceItemEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ItemMapper {
    public static ItemDto toItemDto(InvoiceItemEntity invoiceItemEntity) {
        if (invoiceItemEntity != null) {
            return new ItemDto()
                    .setItemPrice(invoiceItemEntity.getProduct().getPrice())
                    .setProductId(invoiceItemEntity.getProduct().getId())
                    .setProductName(invoiceItemEntity.getProduct().getName())
                    .setQuantity(invoiceItemEntity.getQuantity());
        }
        return null;
    }

    public static List<ItemDto> toItemDto(Set<InvoiceItemEntity> invoiceItemEntities) {
        var items = new ArrayList<ItemDto>();
        if(invoiceItemEntities != null){
            invoiceItemEntities.forEach(item -> items.add(toItemDto(item)));
        }
        return items;
    }
}
