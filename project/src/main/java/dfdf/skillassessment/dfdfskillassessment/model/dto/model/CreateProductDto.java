package dfdf.skillassessment.dfdfskillassessment.model.dto.model;

import lombok.*;
import lombok.experimental.Accessors;

import javax.validation.constraints.*;


@Data
@Accessors(chain = true)
public class CreateProductDto {
    @NotBlank
    @Size(max = 255)
    private String name;

    @NotNull
    @Min(value = 0, message = "Price should not be negative.")
    private double priceHuf;
}
