package dfdf.skillassessment.dfdfskillassessment.model.dto.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ExchangeRateDto {
    private double eurRate;
}
