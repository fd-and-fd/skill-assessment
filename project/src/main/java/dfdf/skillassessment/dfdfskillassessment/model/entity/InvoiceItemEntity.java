package dfdf.skillassessment.dfdfskillassessment.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@Entity
@Table(name = "invoice_item")
@IdClass(InvoiceItemId.class)
public class InvoiceItemEntity {

    @Id
    @Column(name = "invoice_id")
    private int invoiceId;
    @Id
    @Column(name = "product_id")
    private int productId;

    @Column(name = "quantity", nullable = false)
    private int quantity;


    @ManyToOne
    @MapsId("invoiceId")
    @JoinColumn(name = "invoice_id")
    InvoiceEntity invoice;

    @ManyToOne
    @MapsId("productId")
    @JoinColumn(name = "product_id")
    ProductEntity product;
}

