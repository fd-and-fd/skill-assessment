package dfdf.skillassessment.dfdfskillassessment.model.dto.mapper;

import dfdf.skillassessment.dfdfskillassessment.model.dto.model.CreateInvoiceDto;
import dfdf.skillassessment.dfdfskillassessment.model.dto.model.InvoiceDto;
import dfdf.skillassessment.dfdfskillassessment.model.entity.InvoiceEntity;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InvoiceMapper {
    public static InvoiceDto toInvoiceDto(InvoiceEntity invoiceEntity) {
        if (invoiceEntity != null) {
            return new InvoiceDto()
                    .setComment(invoiceEntity.getComment())
                    .setIssueDate(invoiceEntity.getIssueDate())
                    .setDueDate(invoiceEntity.getDueDate())
                    .setCustomerName(invoiceEntity.getCustomerName())
                    .setId(invoiceEntity.getId())
                    .setItems(ItemMapper.toItemDto(invoiceEntity.getItems()));
        }
        return null;
    }

    public static List<InvoiceDto> toInvoiceDto(Iterable<InvoiceEntity> invoiceEntities) {
        var invoices = new ArrayList<InvoiceDto>();
        if (invoiceEntities != null) {
            invoiceEntities.forEach(invoice -> invoices.add(toInvoiceDto(invoice)));
        }
        return invoices;
    }

    public static InvoiceEntity toInvoiceEntity(CreateInvoiceDto createInvoiceDto){
       return new InvoiceEntity()
                .setComment(createInvoiceDto.getComment())
                .setCustomerName(createInvoiceDto.getCustomerName())
                .setDueDate(createInvoiceDto.getDueDate())
                .setIssueDate(createInvoiceDto.getIssueDate());
    }

}
