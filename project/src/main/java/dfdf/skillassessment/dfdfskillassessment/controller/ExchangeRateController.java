package dfdf.skillassessment.dfdfskillassessment.controller;

import dfdf.skillassessment.dfdfskillassessment.clients.IExchangeServiceClient;
import dfdf.skillassessment.dfdfskillassessment.exceptions.ExchangeRateNotAvailableException;
import dfdf.skillassessment.dfdfskillassessment.model.dto.model.ExchangeRateDto;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/exchangerate")
public class ExchangeRateController {

    private final IExchangeServiceClient exchangeService;

    @Autowired
    public ExchangeRateController(IExchangeServiceClient exchangeService) {
        this.exchangeService = exchangeService;
    }

    @ApiResponse(
            responseCode = "200",
            content = @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = @Schema(implementation = ExchangeRateDto.class)))
    @GetMapping
    public ResponseEntity<ExchangeRateDto> getExchangeRate() {
        try {
            return new ResponseEntity<>(
                    exchangeService.getExchangeRate(),
                    HttpStatus.OK);
        } catch (ExchangeRateNotAvailableException e) {
            return ResponseEntity.internalServerError().build();
        }
    }
}
