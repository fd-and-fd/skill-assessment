package dfdf.skillassessment.dfdfskillassessment.controller;

import javax.servlet.http.HttpServletRequest;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Hidden
@Controller
public class SPARoutingController {

    @GetMapping("/**/{path:[^\\.]*}")
	public String forward(HttpServletRequest request) {
        return "forward:/index.html";
    }
}