package dfdf.skillassessment.dfdfskillassessment.controller;

import dfdf.skillassessment.dfdfskillassessment.model.dto.model.CreateProductDto;
import dfdf.skillassessment.dfdfskillassessment.model.dto.model.ProductDto;
import dfdf.skillassessment.dfdfskillassessment.service.ProductService;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService){
        this.productService = productService;
    }

    @ApiResponse(
        responseCode = "200",
        content = @Content(
            mediaType = MediaType.APPLICATION_JSON_VALUE,
            array = @ArraySchema(schema = @Schema(implementation = ProductDto.class))
        )
    )
    @GetMapping("/list")
    public ResponseEntity<List<ProductDto>> getAllProducts() {
        return new ResponseEntity<>(
                productService.listProducts(),
                HttpStatus.OK);
    }

    @ApiResponse(
        responseCode = "200",
        content = @Content(
            mediaType = MediaType.APPLICATION_JSON_VALUE,
            schema = @Schema(implementation = ProductDto.class)
        )
    )
    @PostMapping
    public ResponseEntity<ProductDto> addProduct(@Valid @RequestBody CreateProductDto createProductDto){
        return new ResponseEntity<>(
                productService.createProduct(createProductDto),
                HttpStatus.OK
        );
    }
}
