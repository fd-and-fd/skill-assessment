package dfdf.skillassessment.dfdfskillassessment.controller;

import com.sun.istack.NotNull;
import dfdf.skillassessment.dfdfskillassessment.exceptions.ExchangeRateNotAvailableException;
import dfdf.skillassessment.dfdfskillassessment.exceptions.InvalidDateException;
import dfdf.skillassessment.dfdfskillassessment.model.dto.model.CreateInvoiceDto;
import dfdf.skillassessment.dfdfskillassessment.model.dto.model.InvoiceDto;
import dfdf.skillassessment.dfdfskillassessment.service.InvoiceService;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/invoice")
public class InvoiceController {

    private final InvoiceService invoiceService;

    @Autowired
    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @ApiResponse(
            responseCode = "200",
            content = @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    array = @ArraySchema(schema = @Schema(implementation = InvoiceDto.class))
            )
    )
    @GetMapping("/list")
    public ResponseEntity<List<InvoiceDto>> getAllInvoices() {
        try {
            return new ResponseEntity<>(
                    invoiceService.listInvoices(),
                    HttpStatus.OK);
        } catch (ExchangeRateNotAvailableException e) {
            return ResponseEntity.internalServerError().build();
        }
    }

    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = InvoiceDto.class)
                    )),
            @ApiResponse(responseCode = "400"),
    })
    @GetMapping("/{invoiceId}")
    public ResponseEntity<InvoiceDto> getAllInvoices(@PathVariable @NotNull int invoiceId) {

        try {

            var invoice = invoiceService.getInvoice(invoiceId);
            if (invoice != null) {
                return new ResponseEntity<>(
                        invoice,
                        HttpStatus.OK);
            }
            return ResponseEntity.badRequest().build();

        } catch (ExchangeRateNotAvailableException e) {

            return ResponseEntity.internalServerError().build();
        }
    }

    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = InvoiceDto.class)
                    ))})
    @PostMapping
    public ResponseEntity<InvoiceDto> createInvoice(@RequestBody @Valid CreateInvoiceDto createInvoiceDto) {
        try {
            return new ResponseEntity<>(invoiceService.createInvoice(createInvoiceDto), HttpStatus.OK);
        } catch (InvalidDateException e) {
            return ResponseEntity.badRequest().build();
        } catch (ExchangeRateNotAvailableException e){
            return ResponseEntity.internalServerError().build();
        }
    }
}
