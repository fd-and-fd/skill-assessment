package dfdf.skillassessment.dfdfskillassessment.clients.mnb;

import dfdf.skillassessment.dfdfskillassessment.clients.IExchangeServiceClient;
import dfdf.skillassessment.dfdfskillassessment.clients.mnb.models.GetCurrentExchangeRatesResponse;
import dfdf.skillassessment.dfdfskillassessment.exceptions.ExchangeRateNotAvailableException;
import dfdf.skillassessment.dfdfskillassessment.model.dto.model.ExchangeRateDto;
import org.apache.commons.lang3.StringEscapeUtils;
import org.simpleframework.xml.core.Persister;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;


/*
    WARNING: Hacky solution
    The generated SOAP client is not working, so we are sending the SOAP messages via an HTTPClient.
 */
@Component
public class MnbExchangeServiceClient implements IExchangeServiceClient {

    private static final String RESPONSE_START = "<GetCurrentExchangeRatesResponse";
    private static final String RESPONSE_END = "</s:Body>";

    private static final String MNB_URL = "http://www.mnb.hu/arfolyamok.asmx";
    private static final String MNB_SOAP_ACTION = "http://www.mnb.hu/webservices/MNBArfolyamServiceSoap/GetCurrentExchangeRates";

    private static final String SOAP_MESSAGE = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n" +
            "    <s:Envelope\n" +
            "        xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
            "        <s:Body>\n" +
            "            <GetCurrentExchangeRates\n" +
            "                xmlns=\"http://www.mnb.hu/webservices/\"\n" +
            "                xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\"/>\n" +
            "            </s:Body>\n" +
            "        </s:Envelope>";

    private final HttpClient client;
    private boolean dataIsReady;
    private Date lastUpdated;
    private double eurRate;

    public MnbExchangeServiceClient() {

        client = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .followRedirects(HttpClient.Redirect.NORMAL)
                .connectTimeout(Duration.ofSeconds(20))
                .build();
    }

    @Override
    public ExchangeRateDto getExchangeRate() throws ExchangeRateNotAvailableException {

        updateRateIfNeeded();

        return new ExchangeRateDto()
                .setEurRate(eurRate);
    }

    @Override
    public double exchangeHufToEur(double huf) throws ExchangeRateNotAvailableException {

        updateRateIfNeeded();

        if (eurRate != 0) {
            return round(huf / eurRate);
        }
        return 0;
    }

    private void updateRateIfNeeded() throws ExchangeRateNotAvailableException {
        ZonedDateTime now = ZonedDateTime.now();
        ZonedDateTime thirtyDaysAgo = now.plusDays(-1);

        if (!dataIsReady || lastUpdated.toInstant().isBefore(thirtyDaysAgo.toInstant())) {
            updateEurExchangeRate();
        }
    }

    private void updateEurExchangeRate() throws ExchangeRateNotAvailableException {

        var request = HttpRequest.newBuilder()
                .uri(URI.create(MNB_URL))
                .timeout(Duration.ofMinutes(2))
                .headers("SOAPAction", MNB_SOAP_ACTION, "Content-Type", "text/xml;charset=utf-8")
                .POST(HttpRequest.BodyPublishers.ofString(SOAP_MESSAGE))
                .build();

        try {
            var response = client.send(request, HttpResponse.BodyHandlers.ofString());

            if (response.statusCode() == 200) {
                var body = response.body();

                body = StringEscapeUtils.unescapeHtml4(
                        body.substring(body.indexOf(RESPONSE_START),
                                body.indexOf(RESPONSE_END)));

                var serializer = new Persister();
                var responseModel = serializer
                        .read(GetCurrentExchangeRatesResponse.class, body);

                eurRate = Double.parseDouble(responseModel.
                        getCurrentExchangeRatesResult()
                        .getMnbCurrentExchangeRates()
                        .getRates()
                        .getMap()
                        .get("EUR")
                        .replaceAll(",", "."));

                lastUpdated = java.util.Date
                        .from(LocalDateTime.now().atZone(ZoneId.systemDefault())
                                .toInstant());

                dataIsReady = true;
            }

        } catch (Exception ignored) {
        }

        if (!dataIsReady) {
            throw new ExchangeRateNotAvailableException();
        }
    }

    private static double round(double value) {
        return BigDecimal
                .valueOf(value)
                .setScale(2, RoundingMode.HALF_UP)
                .doubleValue();
    }
}
