package dfdf.skillassessment.dfdfskillassessment.clients.mnb.models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementMap;
import org.simpleframework.xml.Root;

import java.util.Map;

@Root(name="Day")
public class Rates {

    @ElementMap(entry="Rate", key="curr", attribute=true, inline=true)
    private Map<String, String> map;

    @Attribute(name = "date")
    private String date;

    public Map<String, String> getMap() {
        return map;
    }
}

