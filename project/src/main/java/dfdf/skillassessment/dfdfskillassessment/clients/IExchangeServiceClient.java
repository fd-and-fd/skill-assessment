package dfdf.skillassessment.dfdfskillassessment.clients;

import dfdf.skillassessment.dfdfskillassessment.exceptions.ExchangeRateNotAvailableException;
import dfdf.skillassessment.dfdfskillassessment.model.dto.model.ExchangeRateDto;

public interface IExchangeServiceClient {
    ExchangeRateDto getExchangeRate() throws ExchangeRateNotAvailableException;
    double exchangeHufToEur(double huf) throws ExchangeRateNotAvailableException;
}
