package dfdf.skillassessment.dfdfskillassessment.clients.mnb.models;

import lombok.Getter;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Getter
@Root
public class GetCurrentExchangeRatesResponse {
    @Element(name = "GetCurrentExchangeRatesResult")
    private GetCurrentExchangeRatesResult currentExchangeRatesResult;
}

