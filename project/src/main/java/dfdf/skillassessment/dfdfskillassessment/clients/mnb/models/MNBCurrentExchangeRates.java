package dfdf.skillassessment.dfdfskillassessment.clients.mnb.models;

import lombok.Getter;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Getter
@Root
public class MNBCurrentExchangeRates {
    @Element(name = "Day")
    private Rates rates;
}
