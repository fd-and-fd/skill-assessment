package dfdf.skillassessment.dfdfskillassessment.repository;

import dfdf.skillassessment.dfdfskillassessment.model.entity.InvoiceEntity;
import org.springframework.data.repository.CrudRepository;

public interface IInvoiceRepository extends CrudRepository<InvoiceEntity, Integer> {

}
