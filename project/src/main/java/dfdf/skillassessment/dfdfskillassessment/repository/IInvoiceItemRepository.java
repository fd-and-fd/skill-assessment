package dfdf.skillassessment.dfdfskillassessment.repository;

import dfdf.skillassessment.dfdfskillassessment.model.entity.InvoiceItemEntity;
import org.springframework.data.repository.CrudRepository;

public interface IInvoiceItemRepository  extends CrudRepository<InvoiceItemEntity, Integer> {
}
