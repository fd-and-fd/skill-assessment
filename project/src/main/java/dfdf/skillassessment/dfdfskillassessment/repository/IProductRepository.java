package dfdf.skillassessment.dfdfskillassessment.repository;

import dfdf.skillassessment.dfdfskillassessment.model.entity.ProductEntity;
import org.springframework.data.repository.CrudRepository;

public interface IProductRepository extends CrudRepository<ProductEntity, Integer> {
}
