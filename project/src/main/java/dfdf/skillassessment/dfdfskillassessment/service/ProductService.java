package dfdf.skillassessment.dfdfskillassessment.service;

import dfdf.skillassessment.dfdfskillassessment.model.dto.mapper.ProductMapper;
import dfdf.skillassessment.dfdfskillassessment.model.dto.model.CreateProductDto;
import dfdf.skillassessment.dfdfskillassessment.model.dto.model.ProductDto;
import dfdf.skillassessment.dfdfskillassessment.model.entity.ProductEntity;
import dfdf.skillassessment.dfdfskillassessment.repository.IProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    private final IProductRepository productRepository;

    @Autowired
    public ProductService(IProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public ProductDto createProduct(CreateProductDto productDto) {
       var result= productRepository.save(new ProductEntity()
                .setName(productDto.getName())
                .setPrice(productDto.getPriceHuf()));

       return ProductMapper.toProductDto(result);
    }

    public List<ProductDto> listProducts() {
        return ProductMapper.toProductDto(productRepository.findAll());
    }
}
