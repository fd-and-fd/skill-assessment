package dfdf.skillassessment.dfdfskillassessment.service;

import dfdf.skillassessment.dfdfskillassessment.exceptions.ExchangeRateNotAvailableException;
import dfdf.skillassessment.dfdfskillassessment.model.dto.mapper.InvoiceMapper;
import dfdf.skillassessment.dfdfskillassessment.model.dto.model.CreateInvoiceDto;
import dfdf.skillassessment.dfdfskillassessment.model.dto.model.CreateInvoiceItemDto;
import dfdf.skillassessment.dfdfskillassessment.model.dto.model.InvoiceDto;
import dfdf.skillassessment.dfdfskillassessment.model.dto.model.ItemDto;
import dfdf.skillassessment.dfdfskillassessment.model.entity.InvoiceItemEntity;
import dfdf.skillassessment.dfdfskillassessment.model.entity.ProductEntity;
import dfdf.skillassessment.dfdfskillassessment.exceptions.InvalidDateException;
import dfdf.skillassessment.dfdfskillassessment.clients.IExchangeServiceClient;
import dfdf.skillassessment.dfdfskillassessment.repository.IInvoiceItemRepository;
import dfdf.skillassessment.dfdfskillassessment.repository.IInvoiceRepository;
import dfdf.skillassessment.dfdfskillassessment.repository.IProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InvoiceService {

    private final IExchangeServiceClient exchangeService;
    private final IInvoiceRepository invoiceRepository;
    private final IProductRepository productRepository;
    private final IInvoiceItemRepository invoiceItemRepository;

    @Autowired
    public InvoiceService(IInvoiceRepository invoiceRepository,
                          IProductRepository productRepository,
                          IInvoiceItemRepository invoiceItemRepository,
                          IExchangeServiceClient exchangeService) {
        this.invoiceRepository = invoiceRepository;
        this.productRepository = productRepository;
        this.invoiceItemRepository = invoiceItemRepository;
        this.exchangeService = exchangeService;
    }

    public InvoiceDto getInvoice(int id) throws ExchangeRateNotAvailableException {

        var invoice = invoiceRepository.findById(id);
        if (invoice.isPresent()) {
            var invoiceDto = InvoiceMapper.toInvoiceDto(invoice.get());
            calculateTotalPrice(invoiceDto);
            return invoiceDto;
        }
        return null;
    }

    public List<InvoiceDto> listInvoices() throws ExchangeRateNotAvailableException {

        var invoices = InvoiceMapper.toInvoiceDto(invoiceRepository.findAll());
        for (InvoiceDto invoice : invoices) {
            calculateTotalPrice(invoice);
        }
        return invoices;
    }

    public InvoiceDto createInvoice(CreateInvoiceDto createInvoiceDto) throws InvalidDateException, ExchangeRateNotAvailableException {

        if (createInvoiceDto.getIssueDate().compareTo(createInvoiceDto.getDueDate()) > 0) {
            throw new InvalidDateException();
        }

        var invoiceItems = new ArrayList<InvoiceItemEntity>();

        for (CreateInvoiceItemDto item : createInvoiceDto.getItems()) {
            var addProductResult = productRepository.save(new ProductEntity()
                    .setName(item.getProductName())
                    .setPrice(item.getPriceHuf()));

            invoiceItems.add(new InvoiceItemEntity()
                    .setProductId(addProductResult.getId())
                    .setQuantity(item.getQuantity()));
        }

        var invoice = invoiceRepository.save(InvoiceMapper.toInvoiceEntity(createInvoiceDto));

        invoiceItems.forEach(item -> item.setInvoiceId(invoice.getId()));
        invoiceItemRepository.saveAll(invoiceItems);

        return getInvoice(invoice.getId());
    }

    private void calculateTotalPrice(InvoiceDto invoice) throws ExchangeRateNotAvailableException {

        var totalHuf = 0;

        for (ItemDto item : invoice.getItems()) {
            var price = item.getQuantity() * item.getItemPrice();
            item.setTotalHuf(price)
                    .setTotalEur(exchangeService.exchangeHufToEur(price));

            totalHuf += price;
        }

        invoice.setPriceHuf(totalHuf);
        invoice.setPriceEur(exchangeService.exchangeHufToEur(totalHuf));
    }
}
