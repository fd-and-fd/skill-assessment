package dfdf.skillassessment.dfdfskillassessment;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@OpenAPIDefinition(info = @Info(//
		title = "Skill assessment", //
		version = "1.0.0", //
		description = "Skill assessment"//
))
@SpringBootApplication
@RestController
public class DfdfSkillAssessmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(DfdfSkillAssessmentApplication.class, args);
	}

}