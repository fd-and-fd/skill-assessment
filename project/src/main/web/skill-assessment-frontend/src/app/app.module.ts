import { isDevMode, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from './material/material.module';
import { InvoiceListComponent } from './invoice-list/invoice-list.component';
import { ApiModule } from './api/api.module';
import { EllipsisPipe } from './pipes/ellipsis/ellipsis.pipe';
import { InvoiceEntryComponent } from './invoice-entry/invoice-entry.component';
import { CreateInvoiceComponent, CreateInvoiceComponentDialog } from './create-invoice/create-invoice.component';

import { environment } from '../environments/environment';


@NgModule({
  declarations: [
    AppComponent,
    InvoiceListComponent,
    EllipsisPipe,
    InvoiceEntryComponent,
    CreateInvoiceComponent,
    CreateInvoiceComponentDialog
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MaterialModule,
    ApiModule.forRoot({ rootUrl: environment.production ? "http://localhost:8080" : "http://localhost:4200" })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
