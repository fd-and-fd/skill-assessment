import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'ellipsis'
})
export class EllipsisPipe implements PipeTransform {

    transform(value: string, maxLength: number): string {
        if (value === void 0) return value;

        const sliced = value.slice(0, maxLength);
        const ellipses = "...";

        return ((sliced.length + ellipses.length) < value.length)
            ? sliced + ellipses
            : value
        ;
    }

}
