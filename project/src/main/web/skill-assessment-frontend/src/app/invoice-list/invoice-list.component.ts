import { AfterViewInit, Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { merge, Subject } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { InvoiceDto } from '../api/models';
import { InvoiceControllerService } from '../api/services';
import { queryListListenToFirstOfChanges } from '../utils/angular-helpers';
import { columnMapping } from '../utils/column-mapping';

enum COLUMNS {
    CUSTOMER_NAME = "Customer Name",
    ISSUE_DATE = "Issue Date",
    DUE_DATE = "Due Date",
    COMMENT = "Comment",
    TOTAL_HUF = "Total (HUF)",
    TOTAL_EUR = "Total (EUR)"
}

const COLUMN_MAPPING = columnMapping<COLUMNS, InvoiceDto>()({
    [COLUMNS.CUSTOMER_NAME]: "customerName",
    [COLUMNS.ISSUE_DATE]: "issueDate",
    [COLUMNS.DUE_DATE]: "dueDate",
    [COLUMNS.COMMENT]: "comment",
    [COLUMNS.TOTAL_HUF]: "priceHuf",
    [COLUMNS.TOTAL_EUR]: "priceEur",
});

@Component({
    selector: 'app-invoice-list',
    templateUrl: './invoice-list.component.html',
    styleUrls: ['./invoice-list.component.scss']
})
export class InvoiceListComponent implements OnInit, AfterViewInit {

    columns = COLUMNS;
    displayedColumns: COLUMNS[] = [
        COLUMNS.CUSTOMER_NAME,
        COLUMNS.ISSUE_DATE,
        COLUMNS.DUE_DATE,
        COLUMNS.COMMENT,
        COLUMNS.TOTAL_HUF,
        COLUMNS.TOTAL_EUR,
    ];
    columnMapping = COLUMN_MAPPING;

    btn_refresh$ = new Subject<void>();
    ngAfterViewInit$ = new Subject<void>();

    @ViewChildren(MatSort) ql_matSortDirective!: QueryList<MatSort>;
    dataSource = new MatTableDataSource<InvoiceDto>();

    constructor(
        private invoiceController: InvoiceControllerService
    ) {
        const signal_fetch_invoiceList$ = merge(
            this.btn_refresh$,
            this.ngAfterViewInit$
        );

        const fetched_invoiceList$ = signal_fetch_invoiceList$.pipe(
            switchMap(() => this.invoiceController.getAllInvoices1())
        );

        fetched_invoiceList$.subscribe(invoiceList => {
            this.dataSource.data = invoiceList;
        });
    }

    ngOnInit(): void {
        
    }

    ngAfterViewInit(): void {
        queryListListenToFirstOfChanges(this.ql_matSortDirective).subscribe(first => {
            this.dataSource.sort = first;
        });

        /* (:sortingDataAccessor-override) */
        this.dataSource.sortingDataAccessor = (data, sortHeaderId) => {
            const displayedHeader = sortHeaderId as COLUMNS;

            const _data = data as Required<typeof data>;

            const mappedToDataProperty = _data[COLUMN_MAPPING[displayedHeader]];
            return mappedToDataProperty;
        }

        this.ngAfterViewInit$.next();
    }
}
