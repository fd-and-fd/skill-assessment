import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateInvoiceComponent } from './create-invoice/create-invoice.component';
import { InvoiceEntryComponent } from './invoice-entry/invoice-entry.component';
import { InvoiceListComponent } from './invoice-list/invoice-list.component';

export enum ROUTES {
  HOME = "",
  INVOICE_ENTRY = "invoice-entry",
  CREATE_INVOICE = "create-invoice",
}

const routes: Routes = [
  { path: ROUTES.HOME,            component: InvoiceListComponent },
  { path: ROUTES.INVOICE_ENTRY,   component: InvoiceEntryComponent },
  { path: ROUTES.CREATE_INVOICE,  component: CreateInvoiceComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  
}
