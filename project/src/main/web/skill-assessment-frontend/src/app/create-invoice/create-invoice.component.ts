import { AfterViewInit, Component, Inject, OnInit, QueryList, ViewChildren } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Moment } from 'moment';
import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged, map, shareReplay, withLatestFrom } from 'rxjs/operators';
import { CreateInvoiceItemDto, InvoiceDto, ItemDto } from '../api/models';
import { ExchangeRateControllerService, InvoiceControllerService } from '../api/services';
import { ROUTES } from '../app-routing.module';
import { queryListListenToFirstOfChanges } from '../utils/angular-helpers';
import { columnMapping } from '../utils/column-mapping';
import { typesafeKey, OmitKey } from '../utils/types';

enum COLUMNS {
    PRODUCT_NAME =  "Product Name",
    QUANTITY =      "Quantity",
    PRICE_HUF =     "Price (HUF)",
    ACTIONS =       " ",
}

type AugmentedItemDto = ItemDto & { actions?: undefined };
const COLUMN_MAPPING = columnMapping<COLUMNS, AugmentedItemDto>()({
    [COLUMNS.PRODUCT_NAME]: "productName",
    [COLUMNS.QUANTITY]:     "quantity",
    [COLUMNS.PRICE_HUF]:    "itemPrice",
    [COLUMNS.ACTIONS]:      "actions"
});

type CreateInvoiceDialogData = OmitKey<ItemDto, "productId" | "totalHuf" | "totalEur">;
type InvoiceDialogDataInput = {
    eurRate$: Observable<number>
};

type PriceSums = {
    HUF: number,
    EUR: number
};

type SubmitStatus = |(
    | { status: "unsubmitted" }
    | { status: "submitting" }
    | { status: "submitted", resultId: number }
);

@Component({
    selector: 'app-create-invoice',
    templateUrl: './create-invoice.component.html',
    styleUrls: ['./create-invoice.component.scss']
})
export class CreateInvoiceComponent implements OnInit, AfterViewInit {

    private initialFormData = {
        [typesafeKey<InvoiceDto>()("customerName")]: "",
        [typesafeKey<InvoiceDto>()("issueDate")]:    "",
        [typesafeKey<InvoiceDto>()("dueDate")]:      "",
        [typesafeKey<InvoiceDto>()("comment")]:      "",
        [typesafeKey<InvoiceDto>()("items")]:        [] as CreateInvoiceDialogData[]
    };

    invoiceForm = this.formBuilder.group({
        [typesafeKey<InvoiceDto>()("customerName")]: ["", Validators.required],
        [typesafeKey<InvoiceDto>()("issueDate")]:    ["", Validators.required],
        [typesafeKey<InvoiceDto>()("dueDate")]:      ["", Validators.required],
        [typesafeKey<InvoiceDto>()("comment")]:      [""],
        [typesafeKey<InvoiceDto>()("items")]:        this.formBuilder.array([], Validators.required)
    }, {
        validators: dateEndCantBeBeforeStart
    });

    private makeItemGroup(values?: CreateInvoiceDialogData): FormGroup {
        return this.formBuilder.group({
            [typesafeKey<CreateInvoiceDialogData>()("productName")]:    [values?.productName ?? "", Validators.required],
            [typesafeKey<CreateInvoiceDialogData>()("itemPrice")]:      [values?.itemPrice ?? 0, [Validators.required, Validators.min(0)]],
            [typesafeKey<CreateInvoiceDialogData>()("quantity")]:       [values?.quantity ?? 0, [Validators.required, Validators.min(1)]],
        });
    }

    get items(): FormArray {
        return this.invoiceForm.get('items') as FormArray;
    }

    columns = COLUMNS;
    displayedColumns: COLUMNS[] = [
        COLUMNS.PRODUCT_NAME,
        COLUMNS.QUANTITY,
        COLUMNS.PRICE_HUF,
        COLUMNS.ACTIONS
    ];
    columnMapping = COLUMN_MAPPING;

    @ViewChildren(MatSort) ql_matSortDirective!: QueryList<MatSort>;
    dataSource = new MatTableDataSource<AugmentedItemDto>();

    priceSum$ = new BehaviorSubject<PriceSums>({ HUF: 0, EUR: 0 });

    submitStatus$ = new BehaviorSubject<SubmitStatus>({ status: "unsubmitted" });

    private eurRate$(): Observable<number> {
        return new Observable<number>(observer => {
            const sub = this.exchangeRateController.getExchangeRate().pipe(
                map(_ => _.eurRate!)
            ).subscribe(observer);

            return () => sub.unsubscribe();
        }).pipe(
            shareReplay(1)
        );
    }

    constructor(
        private formBuilder: FormBuilder,
        private dialog: MatDialog,
        private invoiceController: InvoiceControllerService,
        private router: Router,
        private exchangeRateController: ExchangeRateControllerService
    ) {
        
    }

    ngOnInit(): void {
        this.submitStatus$.subscribe(submitStatus => {
            if (submitStatus.status === "submitted") {
                this.router.navigate(
                    [ROUTES.INVOICE_ENTRY],
                    { queryParams: { id: submitStatus.resultId } }
                );
            }
        });
    }

    addItem() {
        this.openDialog();
    }
    removeItem(index: number) {
        this.items.removeAt(index);
    }

    onReset() {
        this.invoiceForm.reset(this.initialFormData);
        while ((this.items.length) !== 0) {
            this.items.removeAt(this.items.length - 1);
        }
    }

    onSubmit() {
        this.submitStatus$.next({ status: "submitting" });

        const value = this.invoiceForm.value as InvoiceDto;

        const res$ = this.invoiceController.createInvoice({
            body: {
                customerName: value.customerName!,
                issueDate: value.issueDate!,
                dueDate: value.dueDate!,
                comment: value.comment,
                items: value.items?.map((item): CreateInvoiceItemDto => {
                    return {
                        productName: item.productName!,
                        priceHuf: item.itemPrice,
                        quantity: item.quantity
                    }
                }) ?? new Array<CreateInvoiceItemDto>()
            }
        });

        res$.subscribe(res => {
            this.submitStatus$.next({ status: "submitted", resultId: res.id! });
        });
    }


    ngAfterViewInit(): void {
        queryListListenToFirstOfChanges(this.ql_matSortDirective).subscribe(first => {
            this.dataSource.sort = first;
        });
        
        /* (:sortingDataAccessor-override) */
        this.dataSource.sortingDataAccessor = (data, sortHeaderId) => {
            const displayedHeader = sortHeaderId as COLUMNS;

            const _data = data as Required<typeof data>;

            const mappedToDataProperty = _data[COLUMN_MAPPING[displayedHeader]];
            return mappedToDataProperty;
        };

        const initial = this.initialFormData;
        this.invoiceForm.valueChanges.subscribe((value: typeof initial) => {
            if (value.items !== void 0) {
                const _items = value.items;
                this.dataSource.data = _items;
            }
        });

        this.invoiceForm.valueChanges.pipe(
            map((_: typeof initial) => _.items),
            distinctUntilChanged(),
            withLatestFrom(this.eurRate$())
        ).subscribe(([_items, eurRate]) => {
            this.priceSum$.next(((): PriceSums => {
                const prices = _items.reduce((acc, { itemPrice, quantity }) => {

                    const HUF = itemPrice! * quantity!;
                    const EUR = HUF / eurRate;

                    return {
                        HUF: acc.HUF + HUF,
                        EUR: acc.EUR + EUR,
                    };
                }, { HUF: 0, EUR: 0 });

                return prices;
            })());
        })
    }

    async openDialog(): Promise<void> {

        const dialogRef = this.dialog.open(CreateInvoiceComponentDialog, {
            width: "470px",
            data: {
                eurRate$: this.eurRate$()
            } as InvoiceDialogDataInput
        });

        dialogRef.afterClosed().subscribe((result?: Partial<CreateInvoiceDialogData>) => {
            if (result !== void 0) {
                if (
                    (result.productName !== void 0) &&
                    (result.itemPrice !== void 0) &&
                    (result.quantity !== void 0)
                ) {
                    let { productName, itemPrice, quantity } = result;
    
                    const _result: CreateInvoiceDialogData = {
                        productName, itemPrice, quantity
                    };
    
                    this.items.push(this.makeItemGroup(_result));
                } else {
                    console.warn(
                        "Weirdly, dialog result isn't in the right format!",
                        result
                    );
                }
            }
        });
    }

}

@Component({
    selector: 'app-create-invoice-dialog',
    templateUrl: 'create-invoice-dialog.html',
    styleUrls: ['./create-invoice.component.scss']
})
export class CreateInvoiceComponentDialog implements AfterViewInit {

    itemDialogForm = this.formBuilder.group({
        [typesafeKey<CreateInvoiceDialogData>()("productName")]:    ["", Validators.required],
        [typesafeKey<CreateInvoiceDialogData>()("itemPrice")]:      [null, [Validators.required, Validators.min(0)]],
        [typesafeKey<CreateInvoiceDialogData>()("quantity")]:       [null, [Validators.required, Validators.min(1)]],
    });

    priceSum$ = new BehaviorSubject<PriceSums>({ HUF: 0, EUR: 0 });

    constructor(
        private dialogRef: MatDialogRef<CreateInvoiceComponentDialog>,
        @Inject(MAT_DIALOG_DATA) public data: InvoiceDialogDataInput,
        private formBuilder: FormBuilder
    ) {

    }

    ngAfterViewInit(): void {
        const key_itemPrice =   typesafeKey<CreateInvoiceDialogData>()("itemPrice");
        const key_quantity =    typesafeKey<CreateInvoiceDialogData>()("quantity");

        this.itemDialogForm.valueChanges.pipe(
            withLatestFrom(this.data.eurRate$)
        ).subscribe((
            [value, eurRate]: [{
                [key_itemPrice]: number,
                [key_quantity]: number
            }, number]
        ) => {
            this.priceSum$.next(((): PriceSums => {
                const { itemPrice, quantity } = value;
                
                const HUF = itemPrice! * quantity!;
                const EUR = HUF / eurRate;

                return { HUF, EUR };
            })());
        })
    }

    onCloseDialog(): void {
        this.dialogRef.close(void 0);
    }

    onSubmit(): void {
        this.dialogRef.close(this.itemDialogForm.value);
    }

}

const dateEndCantBeBeforeStart: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const control_issueDate = control.get(typesafeKey<InvoiceDto>()("issueDate"));
    const control_dueDate = control.get(typesafeKey<InvoiceDto>()("dueDate"));

    const issueDate = control_issueDate?.value  as Moment | undefined | "";
    const dueDate   = control_dueDate?.value    as Moment | undefined | "";

    const validMoment = (moment: Moment | undefined | ""): moment is Moment => {
        return (
            (moment !== void 0) &&
            (moment !== "")
        );
    }
    if (
        !validMoment(issueDate) &&
        !validMoment(dueDate)
    ) {
        return { bothDatesAreUndefined: true }
    } else {
        if (!validMoment(issueDate)) {
            return { issueDateIsUndefined: true }
        }
        if (!validMoment(dueDate)) {
            return { dueDateIsUndefined: true }
        }
    }

    const isSameOrAfter = dueDate.isSameOrAfter(issueDate);

    return isSameOrAfter ? null : { issueDate_isNotSameOrAfter_dueDate: true };
};