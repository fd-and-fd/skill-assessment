export type ExtractKey<
    T extends { [key: string]: any },
    U extends keyof T
> =
    U
;
export function typesafeKey<
    T extends { [key: string]: any }
>(): <
    U extends keyof T
>(key: U) => ExtractKey<T, U> {
    return (key) => key;
}

export type OmitKey<
    T extends { [key: string]: any },
    K extends keyof T
> = {
    [P in Exclude<keyof T, K>]: T[P];
}
;

export type ReplaceKey<
    T extends { [key: string]: any },
    U,
    K extends keyof T,
> = {
    [P in keyof T]: P extends K ? U : T[P]
};