import { OperatorFunction } from "rxjs";
import { map } from "rxjs/operators";

export type Initial<T> = {
    initial: boolean,
    value?: T
};

export function mapInitial<T>(): OperatorFunction<T, Initial<T>> {
    return (source) => source.pipe(
        map((value): Initial<T> => ({ initial: false, value }))
    );
};
