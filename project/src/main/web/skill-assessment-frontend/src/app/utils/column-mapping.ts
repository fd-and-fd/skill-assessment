/**
 * For use with `MatTableDataSource<>.sortingDataAccessor`, because the default function
 * implementation assumes that the columns are named the same as the data
 * property names (check function's doc for more info). Instead, we need
 * a mapping between column names and the properties.
 *
 * (:sortingDataAccessor-override)
 */
export function columnMapping<TColumns extends string, TData>():
    <T extends { [K in TColumns]: keyof TData }>(t: T) => T {
        return (t) => t;
    }
;
