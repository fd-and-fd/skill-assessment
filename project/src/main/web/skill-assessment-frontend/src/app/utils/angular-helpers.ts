import { QueryList } from "@angular/core";
import { merge, Observable, of } from "rxjs";
import { filter, map } from "rxjs/operators";

/**
 * Angular may return `undefined` for `queryList.first` if
 * the target element is created by an *ngIf structural directive,
 * we have to listen to `queryList.changes` instead.
 * 
 * However, `queryList.changes` won't emit if there is no *ngIf
 * structural directive.
 * 
 * This is a way to unfify both in one Observable.
 * 
 * Could just choose to use @ViewChild in one case and
 * use @ViewChildren in another, but that is prone to silently
 * breaking if the template changes.
 */
export function queryListListenToFirstOfChanges<T>(
    queryList: QueryList<T>
): Observable<T> {
    let firstMayExist: T | undefined = queryList.first;

    const first$: Observable<T> = merge(
        of(firstMayExist).pipe(filter(_ => _ !== void 0)),
        queryList.changes.pipe(map((ql: QueryList<T>) => ql.first))
    );

    return first$;
}