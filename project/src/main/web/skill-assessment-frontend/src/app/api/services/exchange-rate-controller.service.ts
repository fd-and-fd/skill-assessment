/* tslint:disable */
/* eslint-disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { ExchangeRateDto } from '../models/exchange-rate-dto';

@Injectable({
  providedIn: 'root',
})
export class ExchangeRateControllerService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation getExchangeRate
   */
  static readonly GetExchangeRatePath = '/api/v1/exchangerate';

  /**
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getExchangeRate()` instead.
   *
   * This method doesn't expect any request body.
   */
  getExchangeRate$Response(params?: {
  }): Observable<StrictHttpResponse<ExchangeRateDto>> {

    const rb = new RequestBuilder(this.rootUrl, ExchangeRateControllerService.GetExchangeRatePath, 'get');
    if (params) {
    }

    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<ExchangeRateDto>;
      })
    );
  }

  /**
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getExchangeRate$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getExchangeRate(params?: {
  }): Observable<ExchangeRateDto> {

    return this.getExchangeRate$Response(params).pipe(
      map((r: StrictHttpResponse<ExchangeRateDto>) => r.body as ExchangeRateDto)
    );
  }

}
