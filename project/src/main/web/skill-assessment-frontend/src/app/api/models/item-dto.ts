/* tslint:disable */
/* eslint-disable */
export interface ItemDto {
  itemPrice?: number;
  productId?: number;
  productName?: string;
  quantity?: number;
  totalEur?: number;
  totalHuf?: number;
}
