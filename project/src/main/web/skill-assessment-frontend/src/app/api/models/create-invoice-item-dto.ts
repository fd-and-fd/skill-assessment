/* tslint:disable */
/* eslint-disable */
export interface CreateInvoiceItemDto {
  priceHuf?: number;
  productName: string;
  quantity?: number;
}
