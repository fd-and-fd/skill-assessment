/* tslint:disable */
/* eslint-disable */
import { CreateInvoiceItemDto } from './create-invoice-item-dto';
export interface CreateInvoiceDto {
  comment?: string;
  customerName: string;
  dueDate: string;
  issueDate: string;
  items: Array<CreateInvoiceItemDto>;
}
