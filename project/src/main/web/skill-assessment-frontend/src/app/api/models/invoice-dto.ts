/* tslint:disable */
/* eslint-disable */
import { ItemDto } from './item-dto';
export interface InvoiceDto {
  comment?: string;
  customerName?: string;
  dueDate?: string;
  id?: number;
  issueDate?: string;
  items?: Array<ItemDto>;
  priceEur?: number;
  priceHuf?: number;
}
