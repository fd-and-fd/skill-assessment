To update the generated API code:
  - save the openapi json content to `api-docs.json` in this folder
    - either go to http://localhost:8080/swagger-ui.html and click on the blue link under "Skill assessment"
    - or go to http://localhost:8080/v3/api-docs directly
    - prettifying it is appreciated
  - run `npm run ng-openapi-gen` or just `ng-openapi-gen`, the `ng-openapi-gen` package has to be installed globally
  - adjust frontend code as necessary