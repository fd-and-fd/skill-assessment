/* tslint:disable */
/* eslint-disable */
export interface ProductDto {
  id?: number;
  name?: string;
  priceHuf?: number;
}
