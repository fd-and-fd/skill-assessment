import { AfterViewInit, Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { InvoiceDto, ItemDto } from '../api/models';
import { InvoiceControllerService } from '../api/services';
import { queryListListenToFirstOfChanges } from '../utils/angular-helpers';
import { columnMapping } from '../utils/column-mapping';
import { Initial, mapInitial } from '../utils/initial';

enum COLUMNS {
    PRODUCT_NAME =  "Product Name",
    QUANTITY =      "Quantity",
    PRICE_HUF =     "Price (HUF)",
}

const COLUMN_MAPPING = columnMapping<COLUMNS, ItemDto>()({
    [COLUMNS.PRODUCT_NAME]: "productName",
    [COLUMNS.QUANTITY]:     "quantity",
    [COLUMNS.PRICE_HUF]:    "itemPrice",
});

@Component({
    selector: 'app-invoice-entry',
    templateUrl: './invoice-entry.component.html',
    styleUrls: ['./invoice-entry.component.scss']
})
export class InvoiceEntryComponent implements OnInit, AfterViewInit {

    columns = COLUMNS;
    displayedColumns: COLUMNS[] = [
        COLUMNS.PRODUCT_NAME,
        COLUMNS.QUANTITY,
        COLUMNS.PRICE_HUF,
    ];
    columnMapping = COLUMN_MAPPING;

    private invoiceEntryId$ = new Subject<number>();

    invoice$ = new BehaviorSubject<Initial<InvoiceDto>>({ initial: true });

    @ViewChildren(MatSort) ql_matSortDirective!: QueryList<MatSort>;
    dataSource = new MatTableDataSource<ItemDto>();

    constructor(
        private activatedRoute: ActivatedRoute,
        private invoiceController: InvoiceControllerService
    ) {
        const _invoice$ = this.invoiceEntryId$.pipe(
            switchMap(id => this.invoiceController.getAllInvoices({
                invoiceId: id
            }))
        );

        _invoice$.pipe(mapInitial()).subscribe(invoice => {
            this.invoice$.next(invoice);
            this.dataSource.data = invoice.value?.items!;
        });
    }

    ngOnInit(): void {

    }

    ngAfterViewInit(): void {
        queryListListenToFirstOfChanges(this.ql_matSortDirective).subscribe(first => {
            this.dataSource.sort = first;
        });

        /* (:sortingDataAccessor-override) */
        this.dataSource.sortingDataAccessor = (data, sortHeaderId) => {
            const displayedHeader = sortHeaderId as COLUMNS;

            const _data = data as Required<typeof data>;

            const mappedToDataProperty = _data[COLUMN_MAPPING[displayedHeader]];
            return mappedToDataProperty;
        }

        this.activatedRoute.queryParamMap.subscribe(_ => {
            const str_id = _.get("id");
            if (str_id !== null) {
                const id = Number.parseInt(str_id);
                if (!Number.isNaN(id)) {
                    this.invoiceEntryId$.next(id)
                }
            }
        });
    }

}
