package dfdf.skillassessment.dfdfskillassessment;

import dfdf.skillassessment.dfdfskillassessment.clients.IExchangeServiceClient;
import dfdf.skillassessment.dfdfskillassessment.exceptions.ExchangeRateNotAvailableException;
import dfdf.skillassessment.dfdfskillassessment.exceptions.InvalidDateException;
import dfdf.skillassessment.dfdfskillassessment.model.dto.model.CreateInvoiceDto;
import dfdf.skillassessment.dfdfskillassessment.model.dto.model.CreateInvoiceItemDto;
import dfdf.skillassessment.dfdfskillassessment.model.entity.InvoiceEntity;
import dfdf.skillassessment.dfdfskillassessment.model.entity.InvoiceItemEntity;
import dfdf.skillassessment.dfdfskillassessment.model.entity.ProductEntity;
import dfdf.skillassessment.dfdfskillassessment.repository.IInvoiceItemRepository;
import dfdf.skillassessment.dfdfskillassessment.repository.IInvoiceRepository;
import dfdf.skillassessment.dfdfskillassessment.repository.IProductRepository;
import dfdf.skillassessment.dfdfskillassessment.service.InvoiceService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class InvoiceServiceTests {

    @Mock
    private IExchangeServiceClient exchangeService;
    @Mock
    private IInvoiceRepository invoiceRepository;
    @Mock
    private IProductRepository productRepository;
    @Mock
    private IInvoiceItemRepository invoiceItemRepository;


    @Test
    public void testGetInvoice_Success() throws ExchangeRateNotAvailableException {
        //Arrange
        when(invoiceRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(new InvoiceEntity().setCustomerName("test")));
        when(exchangeService.exchangeHufToEur(Mockito.anyDouble())).thenReturn(350.0);

        var invoiceService = new InvoiceService(invoiceRepository, productRepository, invoiceItemRepository, exchangeService);

        //Act
        var result = invoiceService.getInvoice(1);

        //Assert
        assertEquals(0, result.getPriceHuf());
        assertEquals("test", result.getCustomerName());
    }

    @Test
    public void testListInvoices_Success() throws ExchangeRateNotAvailableException {
        //Arrange
        var items = new ArrayList<InvoiceItemEntity>();
        items.add(new InvoiceItemEntity()
                .setQuantity(10)
                .setProduct(
                        new ProductEntity().
                                setName("testProduct")
                                .setPrice(10)));
        var invoices = new ArrayList<InvoiceEntity>();
        invoices.add(new InvoiceEntity().setCustomerName("test").setItems(new HashSet<>(items)));

        when(invoiceRepository.findAll()).thenReturn(invoices);
        when(exchangeService.exchangeHufToEur(Mockito.anyDouble())).thenReturn(350.0);

        var invoiceService = new InvoiceService(invoiceRepository, productRepository, invoiceItemRepository, exchangeService);

        //Act
        var result = invoiceService.listInvoices();

        //Assert
        assertEquals(result.size(), 1);
        assertEquals(result.stream().findFirst().get().getPriceHuf(), 100);
    }

    @Test
    public void testCreateInvoice_Success() throws ExchangeRateNotAvailableException, InvalidDateException {
        //Arrange
        var items = new ArrayList<CreateInvoiceItemDto>();
        items.add(new CreateInvoiceItemDto()
                .setQuantity(10)
                .setProductName("test")
                .setPriceHuf(100));

        when(invoiceRepository.save(Mockito.any(InvoiceEntity.class))).thenReturn(new InvoiceEntity().setId(1));
        when(productRepository.save(Mockito.any(ProductEntity.class))).thenReturn(new ProductEntity().setId(1));
        when(invoiceRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(new InvoiceEntity().setCustomerName("test").setId(1)));
        when(exchangeService.exchangeHufToEur(Mockito.anyDouble())).thenReturn(350.0);

        var invoiceService = new InvoiceService(invoiceRepository, productRepository, invoiceItemRepository, exchangeService);

        //Act
        var result = invoiceService.createInvoice(new CreateInvoiceDto()
                .setCustomerName("test")
                .setComment("test")
                .setDueDate(convertToDateViaInstant(LocalDate.now()))
                .setIssueDate(convertToDateViaInstant(LocalDate.now()))
                .setItems(items));

        //Assert
        assertEquals(result.getCustomerName(), "test");
    }

    public Date convertToDateViaInstant(LocalDate dateToConvert) {
        return java.util.Date.from(dateToConvert.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }
}
